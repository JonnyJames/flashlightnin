<?php get_header(); ?>
	<div class="main">
		<main role="main">
			<!-- section -->
			<section class="posts">
				<!-- INDEX.PHP -->
				<h1><?php _e( 'Latest Posts', 'html5blank' ); ?></h1>

				<?php get_template_part('loop'); ?>

				<?php get_template_part('pagination'); ?>

			</section>
			<!-- /section -->
		</main>
	</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
