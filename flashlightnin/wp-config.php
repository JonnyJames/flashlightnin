<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'FlashLightnin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n%Wlx3kJKUel 7(/#At+QM6SC~xorBZ~+k|UNaGBP.wm5N1@L%E&+X!^?nv#6>`_');
define('SECURE_AUTH_KEY',  '{1g|:-I5_$tGf2Q{D/)>025D1|-B$]p^C`YQ{Pcsg]@EayG4Pa,iTw[F3tc9|.ZN');
define('LOGGED_IN_KEY',    'vsUq$fUEHmvs|~#_A[]CU$n}K=;Bz$4mmL8e$FcTLEEYb6PJiW+f!C&#D-m-LO-j');
define('NONCE_KEY',        'v%|KZm}<|%n}0i>}*^+2jUc`O1Z|aWEG%8]<1,5OS>eCd?ttCNg@_.F9CJmG<Wz5');
define('AUTH_SALT',        'Tu,na,_>%0nbAn`KHgF-@#w`wlrqaVZ#%Pnx^=.k5.;$;$PX)?Q{v;wqAl]rC[.)');
define('SECURE_AUTH_SALT', '~4_%pW9 Q]gPU?5]~d3/RN 4i#zXtuH?9|DmBkv*/T/}yv=6, NBDKpC&E-|{c(y');
define('LOGGED_IN_SALT',   'O|ddY]dx@zwhsdq.GVB]8F{pdDzAy~_xxow4?D=CG84muE^+PoL4 Xo5biD!OFkB');
define('NONCE_SALT',       '}/X[P&u|Z^3s]o=3kR[T{6%QPo^cq|G`MFAzz#sF9KTh&W2[H+Soddd[Pc]SEXZH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true); 
// ****NOTE:: I changed this from false to true on May 29, 2014. This must be changed back to false, prior to lauching

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
